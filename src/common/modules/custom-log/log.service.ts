import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Log } from 'src/entities/custom-log/log.entity';
import { Repository } from 'typeorm';

@Injectable()
export class LogService {
  constructor(
    @InjectRepository(Log) private readonly logRepository: Repository<Log>,
  ) {}

  async writeLog(
    exception: unknown,
    request: Record<string, any>,
  ): Promise<number> {
    const customException = exception as any;
    const log = new Log();

    log.generatedBy = request.user ? request.user.id : null;
    log.url = request.originalUrl;

    log.detail = `${customException?.response?.error}`;
    log.general = `${exception}`;

    const result = await this.logRepository.save(log);

    return result.id;
  }
}
