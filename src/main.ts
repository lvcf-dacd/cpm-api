import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
import * as helmet from 'helmet';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';
import { adminModules, defaultModules } from './modules/export.module';
import { LogModule } from './common/modules/custom-log/log.module';
import { AllExceptionsFilter } from './common/filters/exception.filter';
import { LogService } from './common/modules/custom-log/log.service';
import { TokenHelper } from './common/helpers/token.helper';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.setGlobalPrefix('api');
  app.enableCors();
  app.use(helmet());

  const configService = app.get(ConfigService);
  const port = configService.get('APP_PORT');

  const options = new DocumentBuilder()
    .setTitle('CPM APIs')
    .setDescription('Detail of CPM APIs')
    .setVersion('1.0')
    .addBearerAuth()
    .build();

  const document = SwaggerModule.createDocument(app, options, {
    include: [...adminModules, ...defaultModules],
    deepScanRoutes: true,
    operationIdFactory: (key: string, method: string) => method,
  });

  SwaggerModule.setup('api/swagger', app, document);

  const logService = app.select(LogModule).get(LogService);

  app.useGlobalFilters(new AllExceptionsFilter(logService));

  const tokenHelper = app.select(AppModule).get(TokenHelper);

  app.useGlobalPipes(
    new ValidationPipe({
      forbidUnknownValues: false,
      skipMissingProperties: true,
      transform: true,
    }),
  );

  await app.listen(port, '0.0.0.0');

  console.log(`api app is working on port: ${port}`);
}
bootstrap();
